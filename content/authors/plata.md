+++
title = "Plata" 
+++


### Who am I?

I'm the developer of the KDE TV guide [Telly Skout](https://apps.kde.org/telly-skout/).

I run Linux with KDE Plasma on my PC, PinePhone and Microsoft Surface Go 2 and try to contribute to the development when I'm missing something.<br>
This includes for example:
- [Telly Skout](https://apps.kde.org/telly-skout/)
- [Alligator](https://apps.kde.org/alligator/)
- [mobile-config-firefox](https://gitlab.com/postmarketOS/mobile-config-firefox)
- [Phoenicis PlayOnLinux](https://github.com/PhoenicisOrg/phoenicis)

I've also written small scripts to help with collecting data for the weekly articles.

### Why do I write here?

I like to share my findings such that others can reuse them. Also, this gives me a place where I can look them up myself ;).
