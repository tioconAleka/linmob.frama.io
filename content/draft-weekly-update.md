+++
title = "Weekly GNU-like Mobile Linux Update (week/year): "
date = "2022-07-17T21:33:00Z"
draft = true 
[taxonomies]
tags = []
categories = ["weekly update"]
authors = ["peter"]
+++
<!-- more -->
_Commentary in italics._

### Hardware enablement

### Software progress

#### GNOME ecosystem

#### Plasma/Maui ecosystem

#### Ubuntu Touch

#### Sailfish OS

#### Maemo Leste

#### Nemo Mobile

#### Capyloon

#### Distributions

### Worth noting

### Worth reading

### Worth listening

### Worth watching

### Something missing?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please get in touch via social media or email!

PS: In case you are wondering about the title [...](https://fosstodon.org/@linmob/108516506484897358)

