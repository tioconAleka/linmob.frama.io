+++
title = "Weekly Update (9/2022): Crickets, Tumbleweeds, Cameras and the Backstory on Sxmo"
date = "2022-03-04T23:22:00Z"
draft = false
[taxonomies]
tags = ["PinePhone","Librem 5","Sxmo","FramebufferPhone","Camera",]
categories = ["weekly update"]
authors = ["peter"]
+++

_Really, not much happened._ But that does not mean that we don't have a nice collection of blog posts, podcasts and videos this weeks. Highlights include a post about the Librem 5 camera, which explains how it can be used and how it got to where it is, a nice guide for writing QML apps with Python, and a video tutorial that explains moving GPG private keys securely, so that you can use your key on your PinePhone.
<!-- more -->

_Commentary in italics._

### Editorial

In an earlier iteration of this I joked that it would be so funny if my Librem 5 1600 days after ordering – and this week I finally received the mail to confirm my address; and later another one that it's likely going to be shipped in the coming two weeks. This is great! I am looking forward to having a second Librem 5 to test (and maybe showcase) distros side-by-side. It's also bad, because it means that I will need to write one or more articles about the device – I promised this years ago.

While writing itself is not necessarily bad, it certainly takes time (and should not be done hastily, as I may have learned again this week (see below)). And time is so scarce, especially now, as I am finally [making progress](https://linmob.uber.space/applist.html) on a road that should lead towards a LinuxPhoneApps.org launch: Fixing LINMOBapps performance was always a major goal.

Hoping that the next mention of that project will be a link to a post on the LinuxPhoneApps.org website, I conclude this editorial with a plea for peace: May the world be as peaceful and boring as these weeks Mobile Linux news are!


### Hardware enablement
* Phoronix: [Imagination Tech Publishes Open-Source PowerVR Vulkan Driver For Mesa](https://www.phoronix.com/scan.php?page=news_item&px=Open-Source-PowerVR-Vulkan). _This is good news going forward._

### Software news

#### GNOME ecosystem

* This Week in GNOME: [#33 Fabulous Screenshots](https://thisweek.gnome.org/posts/2022/03/twig-33/).
* Phoronix: [Cairo Graphics Library Drops Many Old Backends](https://www.phoronix.com/scan.php?page=news_item&px=Cairo-2022-Drops-Old-Code).

#### Plasma/Maui ecosystem
* Decrypted Mind: [January/February in KDE PIM](https://blog.sandroknauss.de/kde-pim-Januar-februar-2022/).
* Phoronix: [Commercial-Only Qt 5.15.3 LTS Now Released As Open-Source](https://www.phoronix.com/scan.php?page=news_item&px=Qt-5.15.3-Open-Source). _Late is better than never, I guess._
* KDAB: [CXX-Qt: Safe Rust Bindings for Qt](https://www.kdab.com/cxx-qt/).
  * Phoronix: [KDAB Launches CXX-Qt To Provide Safe Rust Language Bindings For Qt](https://www.phoronix.com/scan.php?page=news_item&px=CXX-Qt-Rust-Bindings).
* Qt: [QML Type Compilation: that new Qt Quick Compiler part you have not heard much about](https://www.qt.io/blog/qml-type-compilation?utm_source=atom_feed).

#### Distro releases
* [Manjaro ARM with Phosh Beta 23](https://forum.manjaro.org/t/manjaro-arm-beta-23-with-phosh-pinephone-pinephonepro/104120) has been released. _All the new stuff. Meanwhile, Plasma Mobile 5.24 landed in Manjaro ARM stable for PinePhone with Plasma Mobile this week, suggesting that another beta release might be imminent._

#### Non-Linux
* Genode: [ Sculpt OS meets the Pinephone](https://genode.org/documentation/release-notes/22.02#Sculpt_OS_meets_the_Pinephone)


### Worth reading

#### Battery replacements
* Talpa: [Who Needs a Battery?](https://blog.talpa.dk/posts/who-needs-a-battery/). _Fun project!_

#### Minimal PinePhone settings
* Hamblinggreen: [My Pinephone Setup: Why I went minimal](https://hamblingreen.gitlab.io/2022/03/02/my-pinephone-setup.html). _Great first post!_

#### Status of the Librem 5 Camera
* Sebastian Krzyszkowiak for Purism: [Librem 5 Photo Processing Tutorial](https://puri.sm/posts/librem-5-photo-processing-tutorial/). _Great post about how the Librem 5 camera works. In case you were wondering: Yes, it works quite well these days if you take the time to adjust the sliders to your situation._

#### App Development
* Dimitris: [Linux applications with Python and QML](https://dimitris.cc/kde/2022/02/26/pyside-blog-post.html). _Nice guide!_

#### Linux Phone comparisons
* Linux Stans: [Best Linux Phone: All Options Compared](https://linuxstans.com/linux-phone/). _I like round-ups and recognize that they are a lot of work. Please take parts of this with a grain of salt: Not only is it lacking the PinePhone Pro, the claims about FairPhone 4 Linux support [seem a bit optimistic or early](https://wiki.postmarketos.org/wiki/Fairphone_4_(fairphone-fp4)) and [Ubuntu Touch support for the Cosmo Communicator is also not that great](https://devices.ubuntu-touch.io/device/cosmo-communicator/)._

#### Anbox in the Cloud
* Liliputing: [Canonical and Vodafone Cloud Smartphone streams Android apps and games to basic phones](https://liliputing.com/2022/02/canonical-and-vodaphones-cloud-smartphone-streams-android-apps-and-games-to-basic-phones.html). _I don't really know for what this is good for, but hey :) I hope that Anbox Cloud is further along than Android 7._

#### Inflation and other price increases
* Liliputing: [Purism’s Librem 5 Linux smartphone costs $1299 after the latest price hike](https://liliputing.com/2022/03/purisms-librem-5-linux-smartphone-costs-1199-after-the-latest-price-hike.html#comment-1316969). _Tough pricing._

#### PinePhone Modem Upgrading
* LINMOB: [Easily upgrading the PinePhone (Pro) Modem Firmware](https://linmob.net/easily-upgrading-pinephone-pro-modem-firmware/). _I finally managed to the first post this year that's not a Weekly Update. I am not to happy with it though, and will attempt to polish and extend it over the weekend. I don't think changing the opening is a good thing to do, which is quite sad, as it has [already been subject of what I would call mis-interpretation](https://teddit.net/r/pinephone/comments/t6hj1w/exploit_in_pinephone_pro_firmware/).

### Worth listening

* postmarketOS podcast: [#15 INTERVIEW: Miles Alan (of Sxmo, fbp, Mepo Fame)](https://cast.postmarketos.org/episode/15-Interview-Miles-Alan-Sxmo-fbp-Mepo/). _Great episode!_

* Linux Downtime: [Episode 41: Joe is joined by Adam Pigg, a member of the Sailfish OS Community who has ported the OS to various phones](https://latenightlinux.com/linux-downtime-episode-41/).

* Full Circle: [Weekly News #250](https://fullcirclemagazine.org/podcast/full-circle-weekly-news-250/). _Nice weekly news podcast, this week with Ubuntu Touch OTA 22, and a new webOS OSE platform release._

* nephitejnf: [Why Does Data Not Work on my Pinephone Anymore?](https://odysee.com/@nephitejnf:6/ATTPinephone:a9. _This is actually a video, but it's one with a still image. Topic: AT&T being terrible ans what could be done to make this work with the PinePhone._

### Worth watching

#### PinePhone Pro Unboxings
* Bitter Epic Skits: [PinePhone Pro UNBOXING! My first Linux phone!](https://www.youtube.com/watch?v=XROfSJYKGPQ). _This one genuinely made me laugh!_

#### GPG How To
* (RTP) Privacy & Tech Tips: [🔐 Securely Transfer + Encrypt Files + PGP 🔑 Key Backups](https://odysee.com/@RTP:9/%F0%9F%94%90-securely-transfer-+-encrypt-files-+:c). _Well done!_

#### Ubuntu Touch on Tablets
* Massimo Antonio Carofano: [JingPad A1 con Ubuntu Touch](https://www.youtube.com/watch?v=Su1V7LeAOTY).

#### Missed earlier: Linux Gaming
* Heasterian: [Linux desktop games running on Poco F1](https://www.youtube.com/watch?v=zkM3uyc6mZo).

#### Maemo Leste
* Data Portability and Services Incubator: [Maemo Leste project presentation at DAPSI Final Event #2](https://www.youtube.com/watch?v=F5qyTBmwhwE).

#### Ubuntu Touch on the OnePlus One
* Steven Jobs: [Ubuntu touch cannot display wirelessly，unknown reasons](https://www.youtube.com/watch?v=k3HIG9EPo9Q).
* Vitaly Zdanevich: [Ubuntu Touch on OnePlus One](https://www.youtube.com/watch?v=e9OJl_Axr_A).

#### Modem Shorts
* LINMOB.net: [Updating PinePhone (Pro) Modem Firmware with GNOME Firmware Updater](https://www.youtube.com/watch?v=IsFbVZsQJX4).

#### Shorts
* NOT A FBI Honey pot: [running Kali Linux on the pinephone for mobile hacking...](https://www.youtube.com/shorts/XLeiJ7e_s2Q).
* Grad Techie: [First appearance of Ubuntu Touch on Astro Slide 5G #AstroSlide5G #showstoppersmwc #MWC22](https://www.youtube.com/watch?v=_JDeMOQxJsU). _Nice slideshow._

#### Ads
* Purism: [Real Convergence Between Your Computers with PureOS](https://www.youtube.com/watch?v=t-0tZdw3-uc). _We've asked a 100 people about what makes a desktop computing experience a desktop computing experience and 95% answered that resizing windows is the key to a real desktop experience._

### Something missing?
If your projects' cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please get in touch via social media or email!
