+++
title = "Weekly GNU-like Mobile Linux Update (46/2022): A Ubuntu Touch Q&A and progress with mainlining MediaTek phones"
date = "2022-11-20T22:45:00Z"
draft = false
[taxonomies]
tags = ["Sailfish OS","Ubuntu Touch","Phosh","Matrix","MediaTek mainline","Fydetab","PINE64 Community Update",]
categories = ["weekly update"]
authors = ["peter"]
[extra]
author_extra = " (with friendly assistance from plata's awesome script)"
+++

Also: Proper Matrix support landing in Chatty, Neochat getting E2EE and a lot more.
<!-- more -->

_Commentary in italics._

### Hardware enablement
[Jami K.: "#Volla yggdrasil (original #VollaPhone, left) and mimameid (#VollaPhone22, right) powered by #MediaTek MT6763V and MT6769Z SoCs respectively are now both booting close to #mainline #Linux #kernel v6.1-rc5 and #postmarketOS over USB netboot with #Sxmo :)"](https://fosstodon.org/@deathmist/109366195866974426). _Great work!_

### Software progress

#### GNOME ecosystem
- This Week in GNOME: [#70 Useful Progressbars](https://thisweek.gnome.org/posts/2022/11/twig-70/)
- [João Azevedo: "#chatty 0.7.0-rc4 the multi protocol client (sms, matrix, xmpp) used with #phosh on multiple mobile devices, running on a desktop with multiple #matrix accounts and an #xmpp account."](https://social.librem.one/@joao/109354267906979829)

#### Plasma ecosystem	
- Nate Graham: [This week in KDE: less-rage-inducing error messages in Discover](https://pointieststick.com/2022/11/18/this-week-in-kde-less-rage-inducing-error-messages-in-discover/)
- KDE Announcements: [KDE Ships Frameworks 5.100.0](https://kde.org/announcements/frameworks/5/5.100.0/)
- Tobias Fella: [NeoChat, encryption, and thanks for all the olms](https://tobiasfella.de/posts/neochat-e2ee/). _Brilliant news!_
- KDE e.V.: [KDE e.V. is looking for a software engineer](https://ev.kde.org//2022/11/18/cfp-platform/)
- dot.kde.org: [KDE’s New Goals - Join the Kick Off Meeting](https://dot.kde.org/2022/11/16/kde%E2%80%99s-new-goals-join-kick-meeting)
- Volker Krause: [Secure and efficient QNetworkAccessManager use](https://www.volkerkrause.eu/2022/11/19/qt-qnetworkaccessmanager-best-practices.html)

#### Sailfish OS
- [Sailfish Community News, 17th November, the Universe](https://forum.sailfishos.org/t/sailfish-community-news-17th-november-the-universe/13525)
- [Adam Pigg (@adampigg): "With a minor tweak to udev rules ... i _almost_ had the #pinephone with #sailfishos running as a reliable phone. Was able to receive calls from sleep absolutely fine, until after about an hour, the phone wouldnt wake from sleep. So close, just this issue to figure out!"](https://twitter.com/adampigg/status/1592585914320293888)

#### Nemo Mobile
- neochapay on twitter: [#NemoMobile have 10 years old UI/UX (thanks @qwazix ). It's time to renew. Welcome to discussion: https://github.com/nemomobile-ux/main/issues/41 . Without discussion: We do not change the wallpaper! hehehe.](https://twitter.com/neochapay/status/1592493936769380353#m)


#### Ubuntu Touch
- UBports News: [Ubuntu Touch OTA-24 Call for Testing](http://ubports.com/blog/ubports-news-1/post/ubuntu-touch-ota-24-call-for-testing-3867)

#### Distributions
- Manjaro PinePhone Phosh: [Beta 28](https://github.com/manjaro-pinephone/phosh/releases/tag/beta28)
- [tom: "Thanks to Zhaofeng Li for adding a #nixos test for #phosh. This means that PRs to upgrade the Phosh package will be automagically tested in a VM by the NixOS build infra."](https://mastodon.pirateparty.be/@tom/109375172402819839)
- [hamblingreen: "#numen is now available on the AUR for anyone on desktop or #linuxmobile #Arch (or a derivative) looking to easily try out this voice recognition software."](https://fosstodon.org/@hamblingreen/109369289291551115)
- [JF: "My #LoRa driver for the #PineDio USB adapter (and also the #Pinephone backplate) from @PINE64 is now available in #AUR thanks to a contribution from hamblingreen !"](https://mastodon.codingfield.com/@JF/109343849426195283)
- [Adrian Campos Garrido (@hadrianweb): "Time to travel by train to Madrid, I will take advantage of the time to test the latest changes in @openSUSE Updated - Phosh 0.22.0 - Phosh mobile settings 0.22.0 - Feedbackd 0.0.1 - Squeekboard 1.20.0 - Mobile-config-firefox 3.1.0"](https://twitter.com/hadrianweb/status/1592916887197089792)

#### Stack
- Phoronix: [Mesa 22.3 Will Hopefully Be Released Next Week With Improved Vulkan Drivers, Rusticl](https://www.phoronix.com/news/Mesa-22.3-Next-Week)
- Phoronix: [Box64 0.2 Gets DXVK 2.0 Running, Many Other Improvements For Emulating x86_64 On Arm](https://www.phoronix.com/news/Box64-0.2-Box86-0.2.8)
- Phoronix: [SDL 2.26 RC1 Released While SDL3 Development Soon To Get Underway](https://www.phoronix.com/news/SDL-2.26-RC1)

#### Non-Linux
- [Lup Yuen Lee 李立源: "Now merging #PinePhone into Apache #NuttX Mainline"](https://qoto.org/@lupyuen/109364076458191950)

#### Matrix
- Matrix.org: [This Week in Matrix 2022-11-18](https://matrix.org/blog/2022/11/18/this-week-in-matrix-2022-11-18)
- Matrix.org: [Matrix v1.5 release](https://matrix.org/blog/2022/11/17/matrix-v-1-5-release)
- Matrix.org: [Call for Participation for the FOSDEM 2023 Matrix Devroom](https://matrix.org/blog/2022/11/16/call-for-participation-for-the-fosdem-2023-matrix-devroom)

### Worth noting
- Not that it would run now, but the [Mobian Wiki has instructions on building Organic Maps](https://wiki.mobian-project.org/doku.php?id=organicmaps) you might still find interesting.
- [Chris Vogel wrote a note about phosh-ticket-box](https://chrichri.ween.de/o/a4ee24e26d7a4991a868e5ad540849d1), a neat feature for multiple purposes.


### Worth reading
- PINE64: [November Update: Tune(d) in](https://www.pine64.org/2022/11/15/november-update-tuned-in/). _Honestly, if you're looking for fun phone news... you're pretty much out of luck with this Community Update - I suppose they need your contribution to better this situation next time! :-)_
- Phoronix: [Linux 6.1 Adds Support For The Microsoft Surface Pro 9](https://www.phoronix.com/news/Linux-6.1-MS-Surface-Pro-9)
- Purism: [How to Setup Encrypted Chat on Librem Devices](https://puri.sm/posts/how-to-setup-encrypted-chat-on-librem-devices/)
- Purism: [Swinging Back to Open Standards](https://puri.sm/posts/swinging-back-to-open-standards/)
- TuxPhones.com: [GNOME Shell is one step closer to Linux phones](https://tuxphones.com/gnome-shell-43-linux-phone-pinephone-oneplus-performance-video/)
- Pirate Praveen for FSCI: [My adventures with Dino illustrate the importance of Software Freedom](https://fsci.in/blog/importance-of-free-software/). _Make sure to also [check the screenshot of GTK4 Dino](https://social.masto.host/@praveen/109375440761009717) in the fedi._
- Martijn Braam: [Finding an SBC](https://blog.brixit.nl/finding-an-sbc/). _Unsurprisingly, I like lists!_

### Worth listening
- postmarketOS podcast: [#25 INTERVIEW: Alyssa (of Spectrum Fame)](https://cast.postmarketos.org/episode/25-Interview-Alyssa-Spectrum/)

### Worth watching
- UBports: [Ubuntu Touch Q&A 121](https://www.youtube.com/watch?v=y7h9O0yuzvw). _Alfred and Florian are finally back!_
- TechHut: [FINALLY a good Linux tablet - Fydetab Duo](https://www.youtube.com/watch?v=u1pDpzNVnDw). _A nicely made RK3588 Surface-style tablet!_
- Continuum Gaming: [Microsoft Continuum Gaming E339: Sailfish OS: Play Piano with and without AD](https://www.youtube.com/watch?v=pPXcLK8Aa04)
- Martijn Braam: [The Phone Test System](https://spacepub.space/w/gJZ7iAqYYSi6N2KWFadUKC), [YouTube](https://www.youtube.com/watch?v=igH0r4iQ3w4)
- PINE64: [November Update: Tune(d) in](https://www.youtube.com/watch?v=Qr97PEaldts)
- Purism: [How to Setup Encrypted Chat on Librem Devices](https://www.youtube.com/watch?v=93AYk5sO3cQ)

### Thanks

Huge thanks to ollieparanoid for contributing to this installment of the Weekly Update and also to Plata for [a nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speeds up collecting links from feeds by a lot.

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A) for the next one!

PS: In case you are wondering about the title [...](https://fosstodon.org/@linmob/108516506484897358)

PPS: This one has (once again) less headlines - [what do you think?](mailto:weekly-update@linmob.net?subject=Feedback%20on%20Weekly%20Update%2039&body=Less%20or%20more%20headlines%20going%20forward%3F)
